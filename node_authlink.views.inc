<?php

/**
 * Implements hook_views_data().
 */
function node_authlink_views_data() {

  // Table properties.
  $data['node_authlink_nodes']['table']['group'] = t('Node Auth link');

  $data['node_authlink_nodes']['table']['base'] = [
    'field' => 'nid',
    'title' => t('Node auth link nodes table'),
    'help' => t('Table for store authorization keys.'),
    'weight' => -10,
  ];

  $data['node_authlink_nodes']['table']['join'] = [
    'node_field_data' => [
      'left_field' => 'nid',
      'field' => 'nid',
    ],
  ];

  // Table fields.
  $data['node_authlink_nodes']['nid'] = [
    'title' => t('Nid'),
    'help' => t('The node ID.'),
    'relationship' => [
      'label' => t('node'),
      'base' => 'node_field_data',
      'id' => 'standard',
    ],
  ];

  $data['node_authlink_nodes']['authkey'] = [
    'title' => t('Authkey'),
    'help' => t('Authkey field.'),
    'field' => [
      'id' => 'standard',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'standard',
    ],
  ];

  $data['node_authlink_nodes']['created'] = [
    'title' => t('Created date'),
    'help' => t('The date the authkey was created.'),
    'field' => [
      'id' => 'date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  // Node integration.
  $data['node']['node_authlink_nodes'] = [
    'title' => t('Nid'),
    'help' => t('The node ID.'),
    'relationship' => [
      'base' => 'node_authlink_nodes',
      'base field' => 'nid',
      'field' => 'nid',
      'id' => 'standard',
      'label' => t('node authlink'),
      'title' => t('Node Authlink'),
      'help' => t('Relate content to the node authlink table.'),
    ],
  ];

  return $data;
}
