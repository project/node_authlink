<?php


namespace Drupal\node_authlink\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeAccessControlHandler;
use Drupal\node\NodeInterface;

/**
 * Class NodeAuthlinkNodeAccessControlHandler.
 *
 * @package Drupal\node_authlink\Plugin
 */
class NodeAuthlinkNodeAccessControlHandler extends NodeAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $authkey = \Drupal::request()->get('authkey');
    // TODO: generalize to any content entity.
    if (!empty($authkey) && $entity instanceof NodeInterface) {
      if (!$account instanceof AccountInterface) {
        $account = \Drupal::currentUser();
      }
      if (\node_authlink_check_authlink($entity, $operation, $account)) {
        return $return_as_object ? AccessResult::allowed()->addCacheContexts(['url.query_args:authkey']) : TRUE;
      }
    }
    return parent::access($entity, $operation, $account, $return_as_object);
  }

}
