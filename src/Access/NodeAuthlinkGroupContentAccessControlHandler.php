<?php

namespace Drupal\node_authlink\Plugin;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Plugin\GroupContentAccessControlHandler;
use Drupal\node\NodeInterface;

/**
 * Class NodeAuthlinkGroupContentAccessControlHandler.
 *
 * @package Drupal\node_authlink\Access
 */
class NodeAuthlinkGroupContentAccessControlHandler extends GroupContentAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function entityAccess(EntityInterface $entity, $operation, AccountInterface $account, $return_as_object = FALSE) {
    $authkey = \Drupal::request()->get('authkey');
    // TODO: generalize to any content entity.
    if (!empty($authkey) && $entity instanceof NodeInterface) {
      if (node_authlink_check_authlink($entity, $operation, $account)) {
        return $return_as_object ? AccessResult::allowed()->addCacheContexts(['url.query_args:authkey']) : TRUE;
      }
    }

    return parent::entityAccess($entity, $operation, $account, $return_as_object);
  }

}
