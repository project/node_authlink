<?php

namespace Drupal\node_authlink;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Node authlink permissions generator.
 */
class NodeAuthlinkPermissions implements ContainerInjectionInterface
{

  use StringTranslationTrait;

  /**
   * Used to get all the node types.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * NodeAuthlinkPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Permissions callback to granularize node authlink.
   */
  public function permissions() {
    $permissions = [];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $nodeType) {
      $permission_name = sprintf('create and delete node %s authlinks', $nodeType->id());
      $permissions[$permission_name] = [
        'title' => $this->t('Create and delete node "@node_type" authlinks', ['@node_type' => $nodeType->label()])
      ];
    }
    return $permissions;
  }

}
